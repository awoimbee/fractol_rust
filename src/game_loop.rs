use std::sync::atomic::{AtomicBool, Ordering::*};
use std::sync::{Arc, Mutex};
use std::{thread, time};

use crate::input::*;
use crate::vk_render::*;

const PHYSICS_TIME: u64 = 5; // 5ms <=> 200Hz

pub fn game_loop(exit: Arc<AtomicBool>, p_keys: &PKeys, uniform: &Uniform) {
    let u = uniform.get_mut();
    // let mut zoom = 0.5;
    // let mut pos_x = -1.;
    // let mut pos_y = 0.;
    loop {
        let now = time::Instant::now();
        if exit.load(Relaxed) {
            return;
        }
        if p_keys.contains(BTKey::W) {
            u.zoom /= 1.10;
        }
        if p_keys.contains(BTKey::S) && u.zoom < 2. {
            u.zoom *= 1.10;
        }
        if p_keys.contains(BTKey::LFT) {
            u.pos_x -= 0.05 * u.zoom;
        }
        if p_keys.contains(BTKey::RGT) {
            u.pos_x += 0.05 * u.zoom;
        }
        if p_keys.contains(BTKey::UP) {
            u.pos_y -= 0.05 * u.zoom;
        }
        if p_keys.contains(BTKey::DWN) {
            u.pos_y += 0.05 * u.zoom;
        }

        let sleep_dur = match time::Duration::from_millis(PHYSICS_TIME).checked_sub(now.elapsed()) {
            Some(t) => t,
            None => {
                println!("Physics can't keep up !");
                time::Duration::from_secs(0)
            }
        };
        thread::sleep(sleep_dur);
    }
}
